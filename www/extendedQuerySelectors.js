function extendedQuerySelector(params) {
  if (params.includes(":")) {
    var select = params.substring(0, params.indexOf(":"));
    var selected = document.querySelectorAll(select);

    if (params.includes(":eq")) {
      var unformattedEq = params.split(":eq(").pop();
      var eq = unformattedEq.substring(0, unformattedEq.indexOf(")"));
    }

    if (params.includes(":not")) {
      var unformattedNot = params.split(":not(").pop();
      var not = unformattedNot.substring(1, unformattedNot.indexOf(")"));
      var cssSelector = unformattedNot.substring(0, 1);
    }

    for (var i=0; i<selected.length; i++) {
      if (params.includes(":eq") && !params.includes(":not")) {
        return selected[eq];
      } else if (params.includes(":eq") && params.includes(":not")) {
        if (cssSelector === ".") {
          if (selected[eq].className !== not) {
            return selected[eq];
          }
        } else if (cssSelector === "#") {
          if (selected[eq].id !== not) {
            return selected[eq];
          }
        }
      }
    }
  } else {
    return document.querySelector(params);
  }
}

function extendedQuerySelectorAll(params) {
  if (params.includes(":not")) {
    var select = params.substring(0, params.indexOf(":"));
    var selected = document.querySelectorAll(select);

    var results = [];

    var unformattedNot = params.split(":not(").pop();
    var not = unformattedNot.substring(1, unformattedNot.indexOf(")"));
    var cssSelector = unformattedNot.substring(0, 1);

    for (var i=0; i<selected.length; i++) {
      if (cssSelector === ".") {
        if (selected[i].className !== not) {
          results.push(selected[i]);
        }
      } else if (cssSelector === "#") {
        if (selected[i].id !== not) {
          results.push(selected[i]);
        }
      }
    }

    return results;
  } else {
    return document.querySelectorAll(params);
  }
}
