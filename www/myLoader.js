function loadLibs(libs) {
  return new Promise(function(result, error) {

    var selectedLibs = [];

    libs.forEach(function(item) {
      if(selectedLibs.indexOf(item) < 0 && !document.querySelectorAll('[src="'+item+'"]').length > 0) {
        selectedLibs.push(item);
      }
    })

    selectedLibs.forEach(function(item) {
      var s;
      s = document.createElement("script");
      s.src = item;
      s.onload = function() {
        result("Libs loaded");
      };
      s.onerror = function() {
        error("Loading of libs failed");
      };
      document.head.appendChild(s);
    });

  });
}
